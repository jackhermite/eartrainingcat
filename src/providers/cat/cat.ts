import { Injectable } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio';

@Injectable()
export class CatProvider {

  randomNumber_temp : number = null
  leaving_page : boolean = false

  miaous  : any[] = [
    {
      audio_link: 'assets/son/cat/cat1.mp3',
      loaded : false,
      play_finished : true,
    },
    {
      audio_link: 'assets/son/cat/cat2.mp3',
      loaded : false,
      play_finished : true,
    },
    {
      audio_link: 'assets/son/cat/cat3.mp3',
      loaded : false,
      play_finished : true,
    },
    {
      audio_link: 'assets/son/cat/cat4.mp3',
      loaded : false,
      play_finished : true,
    },
    {
      audio_link: 'assets/son/cat/cat5.mp3',
      loaded : false,
      play_finished : true,
    },
    {
      audio_link: 'assets/son/cat/good3.mp3',
      loaded : false,
      play_finished : true,
    },
    {
      audio_link: 'assets/son/cat/good5.mp3',
      loaded : false,
      play_finished : true,
    },
  ] 


  constructor(
    private nativeAudio: NativeAudio,
  ) {
    console.log('Hello CatProvider Provider');
  }

/* -------- Cat random notes in menus -------- */
  catCliked(){
    console.log('catCliked function')
    let randomNumber // Random number selection
    do { 
      randomNumber = Math.floor(Math.random() * this.miaous.length) 
      console.log('randomNumber', randomNumber)
      console.log('this.randomNumber_temp', this.randomNumber_temp)
    }
    while (randomNumber === this.randomNumber_temp)
    this.randomNumber_temp = randomNumber

    let randomMiaou_audio_link // Random miaou audio_link attribution
    randomMiaou_audio_link = this.miaous[randomNumber]['audio_link']
    console.log('randomMiaou_audio_link', randomMiaou_audio_link)
    this.loadPlayUnloadRandomCatSounds(randomNumber, randomMiaou_audio_link)
  }

 
  loadPlayUnloadRandomCatSounds(randomNumber, audio_link){
    console.log('loadPlayUnloadCatSounds')
    this.preloadRandomCatSound(randomNumber, audio_link)
    .then(() => this.playRandomCatSound(randomNumber, audio_link))
    .then(() => this.unloadRandomCatSound(randomNumber, audio_link))
  }

  preloadRandomCatSound(randomNumber, audio_link){
    return new Promise ((resolve, reject) =>{
      if (this.miaous[randomNumber]['loaded'] ===  false ){
        this.nativeAudio.preloadSimple(audio_link, audio_link)
        .then(
          () => {
            console.log('Preload cat sound success')
            this.miaous[randomNumber]['loaded'] =  true
            resolve()
          },
          () => {
            console.log('Preload cat sound  error')
            reject()
          }        
        )
      }else{
        resolve()
      }
    })
  }

  playRandomCatSound(randomNumber, audio_link){
    return new Promise ((resolve, reject) =>{
      this.miaous[randomNumber]['play_finished'] =  false
      this.nativeAudio.play(audio_link)
      .then(
        () => {
          console.log('Play cat sound  success')
          this.miaous[randomNumber]['play_finished'] =  true
          resolve()
        },
        () => {
          console.log('Play cat sound  error')
          this.miaous[randomNumber]['play_finished'] =  true
          reject()
        }        
      )
    })
  }

  unloadRandomCatSound(randomNumber, audio_link){
    console.log('unloadRandomCatSound')
    console.log('this.leaving_page', this.leaving_page)
    return new Promise ((resolve, reject) =>{
      var unload_timer = setTimeout(()=>{
        if (this.miaous[randomNumber]['loaded'] ===  true && this.miaous[randomNumber]['play_finished'] ===  true ){
          this.nativeAudio.unload(audio_link)
          .then(
            () => {
              console.log('Unload cat sound  success')
              this.miaous[randomNumber]['loaded'] =  false
              resolve()
            },
            () => {
              console.log('Unload cat sound  error')
              reject(console.log('Unload cat sound  error'))
            }        
          )
        }else{
          resolve()
        }
      }, 2000)
      if (this.leaving_page){
        console.log('unload_timer')
        clearTimeout(unload_timer);
      }
    })    
  }
  unloadAllRandomCatSound(){
    this.leaving_page = true
    setTimeout(() => this.leaving_page = false,1000)
  }
  /* -------- Cat answer sounds -------- */

  
  checkAnswersInRow(good_in_row, wrong_in_row){
    console.log('loadPlayUnloadCatSounds')
    if(good_in_row === 3) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/good3.mp3')
    if(good_in_row === 5) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/good5.mp3')
    if(good_in_row === 10) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/good10.mp3')
    if(good_in_row === 20) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/good20.mp3')
     
    if(wrong_in_row === 3) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/bad3.mp3')         
    if(wrong_in_row === 5) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/bad5.mp3') 
    if(wrong_in_row === 10) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/bad10.mp3')
    if(wrong_in_row === 20) this.loadPlayUnloadAnswerCatSounds('assets/son/cat/bad20.mp3')
  }

  loadPlayUnloadAnswerCatSounds(audio_link){
    this.preloadCatSound(audio_link)
    .then(() => this.playCatSound(audio_link))
    .then(() => this.unloadCatSound(audio_link))
  }

  preloadCatSound(audio_link){
    return new Promise ((resolve, reject) =>{
      this.nativeAudio.preloadSimple(audio_link, audio_link)
      .then(
        () => {
          console.log('Preload cat sound success')
          resolve()
        },
        () => {
          console.log('Preload cat sound  error')
          reject()
        }        
      )
    })
  }

  playCatSound(audio_link){
    return new Promise ((resolve, reject) =>{
      this.nativeAudio.play(audio_link)
      .then(
        () => {
          console.log('Play cat sound  success, when done')
          resolve()
        },
        () => {
          console.log('Play cat sound  error')
          reject()
        }        
      )
    })
  }

  unloadCatSound(audio_link){
    return new Promise ((resolve, reject) =>{
      setTimeout(()=>{
        this.nativeAudio.unload(audio_link)
        .then(
          () => {
            console.log('Unload cat sound  success')
            resolve()
          },
          () => {
            console.log('Unload cat sound  error')
            reject(console.log('Unload cat sound  error'))
          }        
        )
      }, 2000)
    })    
  }

}
