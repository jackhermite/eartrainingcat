import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Injectable()
export class AlertsProvider {

  constructor(
    public alertCtrl: AlertController,
    private storage: Storage,
  ) {
    storage.ready()
    .then(()=>{
      console.log('Hello AlertsProvider Provider');
      storage.set('show_instruction', true)
      console.log('local storage', this.storage.get('show_instruction').then(val => console.log(val)))
    })
  }

  instructionAlert(){ // Game instruction and first play
    this.storage.ready().then(()=>{
      let show_instruction
      this.storage.get('show_instruction').then(val => show_instruction = val)
      .then(()=>{
        if ( show_instruction === true ){
          let instructions = this.alertCtrl.create({
            title: 'Instructions',
            subTitle: '<p>Two notes are played, the first note played is the tonic, find the second note interval.</p><p> Use replay buttons in case of doubts.</p><p>Bonne chance !',
          })

          instructions.addInput({
            type: 'checkbox',
            label: "Don't show next time",
            value: 'true',
            checked: false
          })

          instructions.addButton({
            text: 'OK',
            handler: data => {
              if (data[0] === 'true'){
                console.log('Checkbox selected')
                this.storage.set('show_instruction', false)
              }
            }
          })

          instructions.present();
        }
      })
    })
  }

  scoreAlert(counter_correct, ratio, points, diatonic_level) {
    return new Promise ((resolve) => {
      let score = this.alertCtrl.create({
        title: 'Your score summary !',
        message: '<p>Correct answers : <strong>' + counter_correct +'/20</strong>, which means a ratio of <strong>' + ratio + '%</strong>.</p><p>So you reached a total score of <strong>'+ points +'</strong>.<p>Tips: to get the highest score avoid to replay notes, go one shot!</p>'
        ,
        buttons: [
          {
            text: 'Back',         
            handler: () => {            
              console.log('Back clicked');
              resolve('Back')          
            }
          },
          {
            text: 'Restart',
            handler: () => {
              console.log('Restart clicked');
              resolve('Restart')             
            }
          },
        ]
      })

      if (diatonic_level < 2){
        score.addButton({
          text: 'Next',
          handler: () => {
            console.log('Next clicked');
            resolve('Next')
          }
        })
      }

      score.present()     
    })

  }

}
