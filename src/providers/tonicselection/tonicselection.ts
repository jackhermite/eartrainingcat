
import { Injectable } from '@angular/core'
import { AlertController } from 'ionic-angular'

@Injectable()
export class TonicselectionProvider {



  constructor( public alertCtrl: AlertController) {
    console.log('Hello TonicselectionProvider Provider')
  }
  showRadio(answers_table) { 
    return new Promise ((resolve, reject) => {
      let alert = this.alertCtrl.create()
      alert.setTitle('Tonic selection')
      alert.addInput({       // Do automaticaly selected
        type: 'radio',
        label: answers_table[12].note,
        value: answers_table[12].tone, 
        checked: true
      }) 
      for(var i =13; i<24;i++){       // Display all other notes
        alert.addInput({
          type: 'radio',
          label: answers_table[i].note,
          value: answers_table[i].tone, 
          checked: false
        })
      }
      alert.addButton('Cancel')
      alert.addButton({
        text: 'OK',
        handler: data => {
          resolve(data)
        }
      })
      alert.present()

    })
  }
}
