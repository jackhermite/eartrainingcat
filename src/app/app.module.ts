import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TonicselectionProvider } from '../providers/tonicselection/tonicselection';
import { AnswersProvider } from '../providers/answers/answers';
import { NativeAudio } from '@ionic-native/native-audio';
import { CatProvider } from '../providers/cat/cat';
import { AlertsProvider } from '../providers/alerts/alerts';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    IonicErrorHandler,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TonicselectionProvider,
    AnswersProvider,
    NativeAudio,
    CatProvider,
    AlertsProvider,
  ]
})
export class AppModule {}
