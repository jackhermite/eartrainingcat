import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiatonicgamePage } from './diatonicgame';

@NgModule({
  declarations: [
    DiatonicgamePage,
  ],
  imports: [
    IonicPageModule.forChild(DiatonicgamePage),
  ],
})
export class DiatonicgamePageModule {}
