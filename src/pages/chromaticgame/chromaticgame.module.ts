import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChromaticgamePage } from './chromaticgame';

@NgModule({
  declarations: [
    ChromaticgamePage,
  ],
  imports: [
    IonicPageModule.forChild(ChromaticgamePage),
  ],
})
export class ChromaticgamePageModule {}
