import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio';
import { Platform } from 'ionic-angular';

//Providers
import { AnswersProvider } from './../../providers/answers/answers';
import { AlertsProvider } from './../../providers/alerts/alerts';
import { CatProvider } from './../../providers/cat/cat';


@IonicPage({
  name: 'chromaticgame'
})
@Component({
  selector: 'page-chromaticgame',
  templateUrl: 'chromaticgame.html',
})
export class ChromaticgamePage {
  //Play button title
  play_title : string = "Start"

  //answers object imported from answers provider
  answers = []
  
  // Level
  chromatic_level : number = 1

  // chromatic intervals tones
  chromatic_interval_set : number[]
  chromatic_intervals  = [[1,2,3,4,5,6,7,8,9,10,11,12],[-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12]]
  
  //Cle portee Sol/G or Fa/F
  cle_portee : string 

  // first_note
  first_note = {}
  random_number_first_note : number
  random_number_first_note_temp : number = null

  //second note
  second_note = {}
  random_interval : number
  random_number_second_note_interval : number // random number for second note draw

  // GamePlay Boolean
  game_started : boolean = false
  game_finished : boolean = false
  answer_given : boolean = false
  game_virgin : boolean = true
  replay_disabled : boolean = true

  //Images portee link
  tonic_img : string 
  answer_img : string 
  
  //Replay counter
  counter_replay : number = 0;

  //Statistics
  counter_total: number = 0;
  counter_correct: number = 0;
  ratio: number;
  points: number = 0;

  //Cat sounds counters
  good_in_row : number = 0
  wrong_in_row : number = 0
  cat_sound : boolean = false

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private answersProvider : AnswersProvider,
    private alertsProvider : AlertsProvider,
    private catProvider : CatProvider,
    private nativeAudio: NativeAudio,
    private platform: Platform,
  ) {
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      console.log('ionViewDidLoad ChromaticgamePage')
      /* -------- Instructions -------- */
      this.alertsProvider.instructionAlert()

      /* -------- Load answers -------- */
      this.answers = this.answersProvider.answers_chromatic
  
      /* -------- Get Level -------- */
      this.chromatic_level = this.navParams.get('chromatic_level')
      console.log('this.chromatic_level', this.chromatic_level )
    })
  }
  
  play(){
    this.booleanReset()
    .then(() => this.getIntervalSet())         /* -------- Get tone intervals according to the level -------- */
    .then(() => this.getFirstNote())           /* -------- Get first note -------- */
    .then(() => this.preloadFirstNoteAudio())  /* -------- Preload first note audio -------- */
    .then(() => this.setTonicImg())            /* -------- get first note img-------- */
    .then(() => this.setAnswers())             /* -------- get first note -------- */
    .then(() => this.getSecondNote())          /* -------- get second note -------- */
    .then(() => this.preloadSecondNoteAudio()) /* -------- Preload second note audio -------- */
    .then(() => this.setAnswerImg())           /* -------- Get second note img -------- */
    .then(() => this.playSep())                /* -------- Play notes separately -------- */
    .catch( error => console.log("Initialisation error :", error))
  }   
  
  getIntervalSet(){
    return new Promise ((resolve) => {
      if (this.chromatic_level === 1){
        this.chromatic_interval_set = this.chromatic_intervals[0]
      }
      else if (this.chromatic_level === 2){
        this.chromatic_interval_set = this.chromatic_intervals[1]
      }
      else {
        let asc_or_desc_intervals = Math.floor(Math.random() * 2)
        console.log('asc_or_desc_intervals', asc_or_desc_intervals)
        this.chromatic_interval_set = this.chromatic_intervals[asc_or_desc_intervals]
      }
      console.log('this.chromatic_level', this.chromatic_level )
      console.log('this.chromatic_interval_set', this.chromatic_interval_set )
      resolve()
    })
  }

  getFirstNote(){
    return new Promise((resolve) => {
      do{ // Random note drawing  
        this.random_number_first_note = Math.floor(Math.random() * 12 + 12 )
      }
      while(this.random_number_first_note_temp === this.random_number_first_note) // avoid repetition
      this.random_number_first_note_temp = this.random_number_first_note

      this.first_note = this.answersProvider.answers_table[this.random_number_first_note] // load first note information
      console.log('first_note', this.first_note)
      resolve()
    })
  }

  preloadFirstNoteAudio(){ // preload sound
    return new Promise((resolve, reject) => {
      console.log('preloadFirstNoteAudio')
      console.log("this.first_note['tone']", this.first_note['tone'])
      console.log("this.first_note['audio_link']", this.first_note['audio_link'])
      
      this.nativeAudio.preloadSimple(this.first_note['tone'], this.first_note['audio_link'])
      .then(
        () => resolve(), 
        () => reject( console.log('preloadFirstNoteAudio error'))
      )
    })
  }
  
  
  setTonicImg(){
    return new Promise ((resolve) =>{
      if(this.chromatic_level === 2 && this.first_note['tone'] < 18 ) {
        this.cle_portee = 'assets/imgs/haut/cle-fa.png'
        this.tonic_img = this.first_note['img_tonic_fa']     
        console.log('Tonic fa img', this.tonic_img)   
      }
      else{
        this.cle_portee = 'assets/imgs/haut/cle-sol.png'
        this.tonic_img = this.first_note['img_tonic_sol']
        console.log('Tonic sol img', this.tonic_img)
      }
      resolve()
    })
  }
  
  setAnswers(){
      for (var i =0 ; i< this.answers.length ; i++){
        this.answers[i].note = this.answersProvider.answers_table[this.random_number_first_note + this.chromatic_interval_set[i]].note
      }
  }

  getSecondNote(){
    return new Promise ((resolve) =>{
      this.random_interval = Math.floor(Math.random() * this.chromatic_interval_set.length) //Random second note
      this.random_number_second_note_interval = this.chromatic_interval_set[this.random_interval] 
      console.log('random_number_second_note_interval', this.random_number_second_note_interval)

      this.second_note = this.answersProvider.answers_table[this.first_note['tone'] + this.random_number_second_note_interval - 1] // load second note information
      console.log('second_note', this.second_note )  

      resolve()
    })
  }

  preloadSecondNoteAudio(){ // preload sound
    return new Promise ((resolve, reject) =>{
      console.log('preloadSecondNoteAudio')
      console.log("this.second_note['tone']", this.second_note['tone'])
      console.log("this.second_note['audio_link']", this.second_note['audio_link'])
  
      this.nativeAudio.preloadSimple(this.second_note['tone'], this.second_note['audio_link']) // preload sound
      .then(
        () => resolve(), 
        () => reject(console.log('preloadSecondNoteAudio error'))
      )
    })
  }

  setAnswerImg(){
    return new Promise ((resolve) =>{
      if(this.chromatic_level === 2 && this.first_note['tone'] < 18 ) {
        this.answer_img = this.second_note['img_ans_fa']  
        console.log('Answer fa img', this.answer_img)      
      }
      else{
        this.answer_img = this.second_note['img_ans_sol']
        console.log('Answer sol img', this.answer_img)   
      }  
      resolve()
    })
  }
  
  playSep(){
    console.log('playSep')
    console.log("this.first_note['tone']", this.first_note['tone'])
    console.log("this.second_note['tone']", this.second_note['tone'])

    this.nativeAudio.play(this.first_note['tone'])
    setTimeout(()=>{this.nativeAudio.play(this.second_note['tone'])}, 700)
  }
  playDuo(){
    console.log('playDuo')
    console.log("this.first_note['tone']", this.first_note['tone'])
    console.log("this.second_note['tone']", this.second_note['tone'])

    this.nativeAudio.play(this.first_note['tone'])
    this.nativeAudio.play(this.second_note['tone'])
  }

  replayDuo(){
    this.playDuo()
    this.counter_replay ++
    if(this.counter_replay > 1) this.replay_disabled = true
  }
  replaySep(){
    this.playSep()
    this.counter_replay ++
    if(this.counter_replay > 1) this.replay_disabled = true
  }

  booleanReset() {
    return new Promise ((resolve) =>{
    if (this.play_title === "Start") this.play_title = "Next"   
    for (var i = 0; i<this.answers.length ; i++) { // Answers colors and effect reset
      this.answers[i].disable_bool= false
      this.answers[i].good_answer= false
      this.answers[i].wrong_answer= false
      this.answers[i].correct_answer= false
      this.answers[i].selected= false
    }
    this.game_started = true // Booleans Reset
    this.game_virgin = false
    this.answer_given = false
    this.replay_disabled = false
    this.counter_replay = 0
      resolve()
    })
  }

  /* -------- Buttons de réponse -------- */
  playerAnswer(answer_selected){
    console.log('playerAnswer')
    /* -------- Update answers state and game booleans -------- */
    for (var i = 0; i<this.answers.length ; i++) this.answers[i].disable_bool= true
    this.answer_given = true
    this.game_started = false
    this.replay_disabled = true
    answer_selected.selected = true

    /* -------- Check player answer -------- */
    if(answer_selected.note === this.second_note['note']){ // good answer
      answer_selected.good_answer = true
      this.counter_correct ++
      this.good_in_row ++
      this.wrong_in_row = 0
      console.log('good_in_row', this.good_in_row)
      if (this.counter_replay === 0) this.points += 5 , console.log ('this.points', this.points)
      if (this.counter_replay === 1) this.points += 4 , console.log ('this.points', this.points)
      if (this.counter_replay === 2) this.points += 2.5 , console.log ('this.points', this.points)

    }
    else{                                                  // wrong answer
      answer_selected.wrong_answer = true
      this.answers[this.random_interval].correct_answer = true
      this.wrong_in_row ++
      this.good_in_row = 0
      console.log('wrong_in_row', this.wrong_in_row)
      
    }

    /* -------- Update statistics -------- */
    this.counter_total ++
    this.ratio = Math.round(this.counter_correct/this.counter_total*100)

    /* -------- Cat sounds -------- */
    this.catProvider.checkAnswersInRow(this.good_in_row, this.wrong_in_row)
    if (this.good_in_row === 3||this.good_in_row ===5||this.good_in_row ===10||this.good_in_row === 20 || this.wrong_in_row === 3 || this.wrong_in_row === 5 || this.wrong_in_row ===10 ||this.wrong_in_row ===20){
      console.log('good in row', this.good_in_row)
      this.cat_sound = true
      setTimeout( () => this.cat_sound = false, 1000)
    }

    /* -------- Unload Notes -------- */
    this.unloadNotes()
    .then(()=>{
      if (this.counter_total >= 20) {
        this.game_finished = true
        setTimeout( () => this.scoreAlertChoice() , 1500)
      }
    })

  }

  // Unloaded the first note
  unloadNotes(){
    return new Promise((resolve) =>{
      console.log('unloadNotes initialisation')
      this.nativeAudio.unload(this.first_note['tone'])
      this.nativeAudio.unload(this.second_note['tone'])
      resolve()
    })
  }

  scoreAlertChoice(){
      this.alertsProvider.scoreAlert(this.counter_correct, this.ratio, this.points, this.chromatic_level)
      .then( alert_choice => {
        console.log('alert_choice', alert_choice)
        if (alert_choice === 'Back') {
          console.log('Back selected')
          this.navCtrl.pop();
        }
        if (alert_choice === 'Restart') {
          console.log('Restart selected')
          this.navCtrl.pop().then(()=>{
            this.navCtrl.push('chromaticgame', {
              chromatic_level: this.chromatic_level
            })
          })
        }
        if (alert_choice === 'Next') {
          console.log('Next level selected')
          this.navCtrl.pop().then(()=>{
            this.navCtrl.push('chromaticgame', {
              chromatic_level: this.chromatic_level + 1
            })
          })
        }
      })
    
  }


/* ----------> When leaving the page  <---------- */   
  ionViewWillLeave() {
    //Answers
    for (var i = 0; i<this.answers.length ; i++) { // Answers colors and effect reset
      this.answers[i].disable_bool= true
      this.answers[i].good_answer= false
      this.answers[i].wrong_answer= false
      this.answers[i].correct_answer= false
      this.answers[i].selected= false
    }
    //Reset statistics
    this.counter_total = 0;
    this.counter_correct = 0;
    this.ratio = 0;

    //Replay counter reset
    this.counter_replay = 0;

    //Unload Notes
    if (this.game_started === true) this.unloadNotes()
  } 
}


