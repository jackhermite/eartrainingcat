import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiatonicPage } from './diatonic';

@NgModule({
  declarations: [
    DiatonicPage,
  ],
  imports: [
    IonicPageModule.forChild(DiatonicPage),
  ],
})
export class DiatonicPageModule {}
