import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArpeggiogamePage } from './arpeggiogame';

@NgModule({
  declarations: [
    ArpeggiogamePage,
  ],
  imports: [
    IonicPageModule.forChild(ArpeggiogamePage),
  ],
})
export class ArpeggiogamePageModule {}
