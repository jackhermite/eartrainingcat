import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CatProvider } from './../../providers/cat/cat';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  interval_list = [
    {
      title : 'Diatonic',
      link_name : 'diatonic',
    },
    {
      title : 'Arpeggio',
      link_name : 'arpeggio',
    },
    {
      title : 'Chromatic',
      link_name : 'chromatic',
    },
  ]
  constructor(public navCtrl: NavController, private catProvider: CatProvider) {

  }

  goToLevelSelection(link_name){
    console.log('link_name', link_name)
    this.navCtrl.push(link_name)
  }
  
  goToTable(){
    this.navCtrl.push('table')
  }  

  /* -------- Cat random notes -------- */
  catMiou(){
    this.catProvider.catCliked()
  }

  ionViewWillLeave() {
    this.catProvider.unloadAllRandomCatSound()
  }
}
