import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChromaticPage } from './chromatic';

@NgModule({
  declarations: [
    ChromaticPage,
  ],
  imports: [
    IonicPageModule.forChild(ChromaticPage),
  ],
})
export class ChromaticPageModule {}
