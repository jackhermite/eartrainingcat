import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CatProvider } from './../../providers/cat/cat';


@IonicPage({
  name : 'chromatic'
})
@Component({
  selector: 'page-diatonic',
  templateUrl: 'chromatic.html',
})
export class ChromaticPage {
  levels: any[] = [
    {
      title : '1 Octave ascending',
      level_number : 1,
    },
    {
      title : '1 Octave descending',
      level_number : 2,
    },
    {
      title : '1 Octave asc. & desc.',
      level_number : 3,
    }]
  constructor(public navCtrl: NavController, public navParams: NavParams, private catProvider: CatProvider) {
  }

  goToChromaticGame(chromatic_level){
    this.navCtrl.push('chromaticgame', {
      chromatic_level: chromatic_level
    })
  }  
  /* -------- Cat random notes -------- */
  catMiou(){
    this.catProvider.catCliked()
  }

  ionViewWillLeave() {
    this.catProvider.unloadAllRandomCatSound()
  }
}

