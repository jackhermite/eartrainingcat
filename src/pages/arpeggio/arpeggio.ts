import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CatProvider } from './../../providers/cat/cat';

@IonicPage({
  name : 'arpeggio'
})
@Component({
  selector: 'page-arpeggio',
  templateUrl: 'arpeggio.html',
})
export class ArpeggioPage {
  levels: any[] = [
    {
      title : '1 Octave ascending',
      level_number : 1,
    },
    {
      title : '1 Octave descending',
      level_number : 2,
    },
    {
      title : '1 Octave asc. & desc.',
      level_number : 3,
    }]
  constructor(public navCtrl: NavController, public navParams: NavParams, private catProvider: CatProvider) {

  }

  goToArpeggioGame(arpeggio_level){
    this.navCtrl.push('arpeggiogame', {
      arpeggio_level: arpeggio_level
    })
  }    
  /* -------- Cat random notes -------- */
  catMiou(){
    this.catProvider.catCliked()
  }

  ionViewWillLeave() {
    this.catProvider.unloadAllRandomCatSound()
  }
}
