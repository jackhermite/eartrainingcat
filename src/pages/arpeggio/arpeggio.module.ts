import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArpeggioPage } from './arpeggio';

@NgModule({
  declarations: [
    ArpeggioPage,
  ],
  imports: [
    IonicPageModule.forChild(ArpeggioPage),
  ],
})
export class ArpeggioPageModule {}
