import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnswersProvider } from './../../providers/answers/answers';
import { TonicselectionProvider } from './../../providers/tonicselection/tonicselection';
import { NativeAudio } from '@ionic-native/native-audio';
import { Platform } from 'ionic-angular';

@IonicPage({
  name : 'table'
})
@Component({
  selector: 'page-table',
  templateUrl: 'table.html',
})
export class TablePage {
  //Tonic
  tonic : any = {
    tone :	13	,
    note :	'Do/C'	,
    audio_link :	'assets/son/do1.mp3'	,
  }
  tonic_tone : number

  //selected interval
  interval : any = {

  }

  interval_playing : boolean = false
  interval_loaded : boolean = false
  leaving_page : boolean = false
  unload_timer

  //Intervals list
  answers_table = this.answersProvider.answers_table
  ascending_notes = this.answersProvider.ascending_notes
  descending_notes = this.answersProvider.descending_notes

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    private tonicselectionProvider: TonicselectionProvider, 
    private answersProvider : AnswersProvider,
    private nativeAudio: NativeAudio,
    private platform: Platform,
  ) 
  {
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      console.log('ionViewDidLoad TablePage');
      this.preloadTonic()
    })

  }

  preloadTonic(){
    return new Promise ((resolve, reject) => {
      console.log('preloadTonic')
      this.nativeAudio.preloadSimple(this.tonic['tone'], this.tonic['audio_link'])
      .then(
        () => resolve(), 
        () => reject( console.log('preloadTonic error') )
      )
    })
  }

  selectTonic(){
    //Unload notes to add
    this.unloadTonic() // Unload tonic audio
    .then( () => this.tonicselectionProvider.showRadio(this.answers_table))
    .then( data => { // Get new tonic
      console.log('Data value :', data)
      this.tonic_tone = Number(data)
      this.tonic = this.answers_table[this.tonic_tone -1]
      console.log('this.tonic', this.tonic)
    })
    .then( () => this.preloadTonic()) // Preload tonic audio
    .then( () =>{ // display corresponding interval notes
      for(var i = 1 ; i < 13; i++){ // Ascending notes
        this.ascending_notes[12-i]['tone'] = this.answers_table[this.tonic_tone + i -1]['tone']
        this.ascending_notes[12-i]['interval_note'] = this.answers_table[this.tonic_tone + i -1]['note']
        this.ascending_notes[12-i]['audio_link'] = this.answers_table[this.tonic_tone + i -1]['audio_link']
      }
      for(var j = 0 ; j < 12; j++){ // Descending notes
        this.descending_notes[j]['tone'] = this.answers_table[this.tonic_tone - j - 2]['tone']
        this.descending_notes[j]['interval_note']  = this.answers_table[this.tonic_tone - j - 2]['note']
        this.descending_notes[j]['audio_link'] = this.answers_table[this.tonic_tone -j -2]['audio_link']
      }
    })
    .catch( error => console.log("selectTonic error :", error))
  }

  unloadTonic(){
    return new Promise ((resolve, reject) => {
      this.nativeAudio.unload(this.tonic.tone)
      .then(
        () => resolve(), 
        () => reject( console.log('unloadFirstNoteAudio error'))
      )
    })    
  }


  processInterval(interval_selected){
    console.log('interval_selected', interval_selected)
    console.log('interval_selected.tone', interval_selected['tone'])
    console.log('interval_selected.audio_link', interval_selected['audio_link'])
    this.interval_playing = true
    interval_selected.selected = true
    this.setIntervalValues(interval_selected)
    .then(() => this.preloadInterval())
    .then(() => this.playInterval())
    .then(() => this.unloadInterval(interval_selected))
    .catch(error => console.log ('error :', error))
  }


  setIntervalValues(interval_selected){
    return new Promise ((resolve) =>{
      this.interval['tone'] = interval_selected['tone']
      this.interval['audio_link'] = interval_selected['audio_link']
      console.log("this.interval['tone']", this.interval['tone'])
      console.log("this.interval['audio_link']", this.interval['audio_link'])
      resolve()
    })
  }

  preloadInterval(){
    return new Promise ((resolve, reject) =>{
      console.log("this.interval['tone']", this.interval['tone'])
      console.log("this.interval['audio_link']", this.interval['audio_link'])
      this.nativeAudio.preloadSimple(this.interval['tone'], this.interval['audio_link'])
      .then(
        () => {
          console.log('Preload inteval success')
          this.interval_loaded = true
          resolve()
        },
        () => {
          console.log('Preload inteval error')
          reject()
        }        
      )
    })
  }

  playInterval(){
    return new Promise ((resolve, reject) =>{
      console.log("this.interval['tone']", this.interval['tone'])
      console.log("this.interval['audio_link']", this.interval['audio_link'])
      
      setTimeout(()=>{this.nativeAudio.play(this.interval['tone'])}, 700)
      this.nativeAudio.play(this.tonic['tone'])
      .then(
        () => {
          console.log('Play inteval success')
          resolve()
        },
        () => {
          console.log('Play inteval error')
          reject()
        }        
      )
    })
  }

  unloadInterval(interval_selected){
    return new Promise ((resolve, reject) =>{
      console.log("this.interval['tone']", this.interval['tone'])
      console.log("this.interval['audio_link']", this.interval['audio_link'])
      setTimeout( () =>{
        this.nativeAudio.unload(this.interval['tone'])
        .then(
          () => {
            console.log('Unload interval success')
            this.interval_loaded = false           
            this.interval_playing = false
            interval_selected.selected = false
            resolve()
          },
          () => {
            console.log('Unload interval error')
            reject(console.log('Unload interval error'))
          }        
        )
      }, 2000)
    })    
  }

/* ----------> When leaving the page  <---------- */   
  ionViewWillLeave() {
    this.nativeAudio.unload(this.tonic['tone'])
  }
}
